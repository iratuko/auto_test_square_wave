#include <nidaqmxbase/NIDAQmxBase.h>
#include <iostream>
#include <cmath>
#include <chrono>
#include <string>
#include <fstream>
#include <unistd.h>
#include <termios.h>
#include <fcntl.h>
#define int64_t /*opencvとNIdaqmxbaseを使用するときはこれを書くこと*/
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "kbhit.hpp"
#define _USE_MATH_DEFINES
#define PI M_PI
#define bufferSize 1000
#define bufferSize_Freq 40000 //倍率５倍
#define bufferSize_Set 1000
#define bufferSize_Tim 1000
#define LIMIT 10
#define COUNTER 0.1
#define LoopNum 8
#define LoopNum_Freq 1
#define LoopNum_Set 5
#define LoopNum_Tim 36
#define bufferSize_FreqC 40000
#define LoopNum_FreqC 32
#define bufferSize_Freqx 40000
#define LoopNum_Freqx 10

struct Task{
    TaskHandle  taskHandle;
    float64     min;
    float64     max;
    float64     sampleRate;
    float64     timeout;
    uInt32      samplesPerChan;
    int32       pointsWritten;
    char        *chan;
    int         test;
}task;
void init(){
    task.taskHandle = 0;
    task.min = 0;
    task.max = LIMIT;
    task.sampleRate = 200000.0; //"samplRate/samplePerChan=Hz",samplRateはsamplePerChan倍以上
    task.timeout = 10.0;
    task.samplesPerChan = bufferSize;
    task.pointsWritten = 0;
    task.chan = "Dev1/ao0,Dev1/ao1";
    task.test = 1200;
}
void ni_make(float64 *data){
    DAQmxBaseResetDevice(task.chan);
    DAQmxBaseStopTask(task.taskHandle);
    DAQmxBaseClearTask(task.taskHandle);
    DAQmxBaseCreateTask("",&task.taskHandle);
    DAQmxBaseCreateAOVoltageChan(task.taskHandle,task.chan,"",task.min,task.max,DAQmx_Val_Volts,NULL);
    DAQmxBaseCfgSampClkTiming(task.taskHandle,"",task.sampleRate,DAQmx_Val_Rising,DAQmx_Val_ContSamps,task.samplesPerChan);
    DAQmxBaseSetWriteAttribute(task.taskHandle, DAQmx_Write_RegenMode, DAQmx_Val_DoNotAllowRegen);
    DAQmxBaseWriteAnalogF64(task.taskHandle,task.samplesPerChan,0,task.timeout,DAQmx_Val_GroupByChannel,data,&task.pointsWritten,NULL);
    DAQmxBaseStartTask(task.taskHandle);
}
float64 SquareWave_freq_Change[LoopNum_FreqC][bufferSize_FreqC*2];
float64 SquareWave_freq_X[LoopNum_Freqx][bufferSize_Freqx*2];
int main(int argc, char *argv[])
{
    init();
    float64 ZeroVolData[bufferSize*2] = {0};
    std::cout << "データ作成開始" << std::endl;
    std::cout << "データ作成中" << std::endl;
    
    std::cout << "Tim_Square_wave" << std::endl;
    float64 Tim_Square_wave[LoopNum_Tim][bufferSize_Tim*2];
    
    double CH1_Tim[LoopNum_Tim][8]={
        {3,3,3,3,0,0,0,0},
        {0,3,3,3,3,0,0,0},
        {0,0,3,3,3,3,0,0},
        {0,0,0,3,3,3,3,0},
        {0,0,0,0,3,3,3,3},
        {3,0,0,0,0,3,3,3},
        {3,3,0,0,0,0,3,3},
        {3,3,3,0,0,0,0,3},
        {3,3,3,3,0,0,0,0},
        
        {3,3,3,3,0,0,0,0},
        {3,3,3,3,0,0,0,0},
        {3,3,3,3,0,0,0,0},
        {3,3,3,3,0,0,0,0},
        {3,3,3,3,0,0,0,0},
        {3,3,3,3,0,0,0,0},
        {3,3,3,3,0,0,0,0},
        {3,3,3,3,0,0,0,0},
        {3,3,3,3,0,0,0,0},
        
        {3,3,3,3,0,0,0,0},
        {3,3,3,3,0,0,0,0},
        {3,3,3,3,0,0,0,0},
        {3,3,3,3,0,0,0,0},
        {3,3,3,3,0,0,0,0},
        {3,3,3,3,0,0,0,0},
        {3,3,3,3,0,0,0,0},
        {3,3,3,3,0,0,0,0},
        {3,3,3,3,0,0,0,0},
        
        {3,3,3,3,0,0,0,0},
        {3,3,3,0,0,0,0,3},
        {3,3,0,0,0,0,3,3},
        {3,0,0,0,0,3,3,3},
        {0,0,0,0,3,3,3,3},
        {0,0,0,3,3,3,3,0},
        {0,0,3,3,3,3,0,0},
        {0,3,3,3,3,0,0,0},
        {3,3,3,3,0,0,0,0},
        
    
    };
    double CH2_Tim[LoopNum_Tim][8] = {
        {3,3,3,3,0,0,0,0},
        {3,3,3,3,0,0,0,0},
        {3,3,3,3,0,0,0,0},
        {3,3,3,3,0,0,0,0},
        {3,3,3,3,0,0,0,0},
        {3,3,3,3,0,0,0,0},
        {3,3,3,3,0,0,0,0},
        {3,3,3,3,0,0,0,0},
        {3,3,3,3,0,0,0,0},
        
        {3,3,3,3,0,0,0,0},
        {3,3,3,0,0,0,0,3},
        {3,3,0,0,0,0,3,3},
        {3,0,0,0,0,3,3,3},
        {0,0,0,0,3,3,3,3},
        {0,0,0,3,3,3,3,0},
        {0,0,3,3,3,3,0,0},
        {0,3,3,3,3,0,0,0},
        {3,3,3,3,0,0,0,0},
        
        {3,3,3,3,0,0,0,0},
        {0,3,3,3,3,0,0,0},
        {0,0,3,3,3,3,0,0},
        {0,0,0,3,3,3,3,0},
        {0,0,0,0,3,3,3,3},
        {3,0,0,0,0,3,3,3},
        {3,3,0,0,0,0,3,3},
        {3,3,3,0,0,0,0,3},
        {3,3,3,3,0,0,0,0},
        
        {3,3,3,3,0,0,0,0},
        {3,3,3,3,0,0,0,0},
        {3,3,3,3,0,0,0,0},
        {3,3,3,3,0,0,0,0},
        {3,3,3,3,0,0,0,0},
        {3,3,3,3,0,0,0,0},
        {3,3,3,3,0,0,0,0},
        {3,3,3,3,0,0,0,0},
        {3,3,3,3,0,0,0,0}
    
    };
    int xt = 0;
    while (1) {
        for (int i = 0; i < bufferSize_Tim; i++) {
            if (0 <= i && i <bufferSize_Tim/8) {
                Tim_Square_wave[xt][i] = CH1_Tim[xt][0];
                Tim_Square_wave[xt][i+bufferSize_Tim] = CH2_Tim[xt][0];
            }
            else if (bufferSize_Tim/8 <= i && i < 2*bufferSize_Tim/8) {
                Tim_Square_wave[xt][i] = CH1_Tim[xt][1];
                Tim_Square_wave[xt][i+bufferSize_Tim] = CH2_Tim[xt][1];
            }
            else if (2*bufferSize_Tim/8 <= i && i < 3*bufferSize_Tim/8) {
                Tim_Square_wave[xt][i] = CH1_Tim[xt][2];
                Tim_Square_wave[xt][i+bufferSize_Tim] = CH2_Tim[xt][2];
            }
            else if (3*bufferSize_Tim/8 <= i && i < 4*bufferSize_Tim/8) {
                Tim_Square_wave[xt][i] = CH1_Tim[xt][3];
                Tim_Square_wave[xt][i+bufferSize_Tim] = CH2_Tim[xt][3];
            }
            else if (4*bufferSize_Tim/8 <= i && i <5*bufferSize_Tim/8) {
                Tim_Square_wave[xt][i] = CH1_Tim[xt][4];
                Tim_Square_wave[xt][i+bufferSize_Tim] = CH2_Tim[xt][4];
            }
            else if (5*bufferSize_Tim/8 <= i && i < 6*bufferSize_Tim/8) {
                Tim_Square_wave[xt][i] = CH1_Tim[xt][5];
                Tim_Square_wave[xt][i+bufferSize_Tim] = CH2_Tim[xt][5];
            }
            else if (6*bufferSize_Tim/8 <= i && i < 7*bufferSize_Tim/8) {
                Tim_Square_wave[xt][i] = CH1_Tim[xt][6];
                Tim_Square_wave[xt][i+bufferSize_Tim] = CH2_Tim[xt][6];
            }
            else if (7*bufferSize_Tim/8 <= i && i < bufferSize_Tim) {
                Tim_Square_wave[xt][i] = CH1_Tim[xt][7];
                Tim_Square_wave[xt][i+bufferSize_Tim] = CH2_Tim[xt][7];
            }
        }
        
        xt++;
        if (xt == LoopNum_Tim) {
            std::cout << "" << std::endl;
            break;
        }
    }
    
    std::cout << "Set_Square_wave" << std::endl;
    float64 Set_Square_wave[LoopNum_Set][bufferSize_Set*2];
    int x = 0;
        double Ch1_Vol_Front[5] = {3,0,3,3,0};
        double Ch1_Vol_Behind[5] = {0,0,0,0,3};
        double Ch2_Vol_Front[5] = {0,3,3,0,3};
        double Ch2_Vol_Behind[5] = {0,0,0,3,0};
    while (1) {
        for (int i = 0; i<bufferSize_Set; i++) {
            if (i <= bufferSize_Set/2) {
                Set_Square_wave[x][i] = Ch1_Vol_Front[x];
                Set_Square_wave[x][i+bufferSize_Set] = Ch2_Vol_Front[x];
            }else{
                Set_Square_wave[x][i] = Ch1_Vol_Behind[x];
                Set_Square_wave[x][i+bufferSize_Set] = Ch2_Vol_Behind[x];
            }
        }
        x++;
        if (x == 5) {
            std::cout << "break" << std::endl;
            break;
        }
    }
    
    std::cout << "SquareWave" << std::endl;
    int n = 0;
    float64 **SquareWave;
    SquareWave = new float64*[LoopNum];
    for (int i = 0; i<LoopNum; i++) {
        SquareWave[i] = new float64[(task.test-(i*10))*2];
    }
    while (1) {
        for (int i = 0; i < task.test-(n*10); i++) {
            if ((task.test-(n*10))/2 > i) {
                SquareWave[n][i] = 2.5;
                SquareWave[n][i+task.test-(n*10)] = 2.5;
            }
            if ((task.test-(n*10))/2 <= i) {
                SquareWave[n][i] = 0;
                SquareWave[n][i+task.test-(n*10)] = 0;
            }
        }
        n++;
        if (n == LoopNum) {
            std::cout << "break" << std::endl;
            break;
        }
    }
    
    
    std::cout << "SquareWave_freq" << std::endl;
    float64 SquareWave_freq[LoopNum_Freq][bufferSize_Freq*2];
    int ct = 0;
    int ch1 = 64;
    int ch2 = 32;
    //64~32まで，フォースリアクタ
    for (int m = 0; m<ch1; m++) {
        for (int i = 0; i<bufferSize_Freq/ch1; i++) {
            if (i>=bufferSize_Freq/(2*ch1)) {
                SquareWave_freq[ct][i+(bufferSize_Freq/ch1)*m] = 3;
            }
            else {
                SquareWave_freq[ct][i+(bufferSize_Freq/ch1)*m] = 0;
            }
        }
    }
    for (int m = 0; m<ch2; m++) {
        for (int i = bufferSize_Freq; i < bufferSize_Freq+bufferSize_Freq/ch2; i++) {
            if (i < bufferSize_Freq+bufferSize_Freq/(2*ch2)) {
                SquareWave_freq[ct][i+(bufferSize_Freq/ch2)*m] = 3;
            }else{
                SquareWave_freq[ct][i+(bufferSize_Freq/ch2)*m] = 0;
            }
        }
    }

    
    std::cout << "SquareWave_freq_Change" << std::endl;
    
    int cf = 0;
    int CHf1 = 64;
    int CHf2 = 32;
    
    while (1) {
        
        for (int m = 0; m<CHf1; m++) {
            for (int i = 0; i<bufferSize_FreqC/CHf1; i++) {
                if (i>=bufferSize_FreqC/(2*CHf1)) {
                    SquareWave_freq_Change[cf][i+(bufferSize_FreqC/CHf1)*m] = 3;
                }
                else {
                    SquareWave_freq_Change[cf][i+(bufferSize_FreqC/CHf1)*m] = 0;
                }
            }
        }
        for (int m = 0; m<CHf2; m++) {
            for (int i = bufferSize_FreqC; i < bufferSize_FreqC+bufferSize_FreqC/CHf2; i++) {
                if (i < bufferSize_Freq+bufferSize_FreqC/(2*CHf2)) {
                    SquareWave_freq_Change[cf][i+(bufferSize_FreqC/CHf2)*m] = 3;
                }
                else{
                    SquareWave_freq_Change[cf][i+(bufferSize_FreqC/CHf2)*m] = 0;
                }
            }
        }
        std::cout << cf << std::endl;
        cf++;
        CHf1--;
        CHf2++;
        if (cf == LoopNum_FreqC) {
            std::cout << "break FreqC" << std::endl;
            break;
        }
    }
//    std::cout << "SquareWave_freq_X" << std::endl;
//    
//    int cfx = 0;
//    int CHfx1 = 64;
//    int CHfx2 = 32;
//    
//    while (1) {
//        for (int m = 0; m<CHfx1; m++) {
//            for (int i = 0; i<bufferSize_FreqC/CHfx1; i++) {
//                if (i>=bufferSize_FreqC/(2*CHfx1)) {
//                    SquareWave_freq_X[cfx][i+(bufferSize_FreqC/CHfx1)*m] = 3;
//                }
//                else {
//                    SquareWave_freq_X[cfx][i+(bufferSize_FreqC/CHfx1)*m] = 0;
//                }
//            }
//        }
//        for (int m = 0; m<CHfx2; m++) {
//            for (int i = bufferSize_FreqC; i < bufferSize_FreqC+bufferSize_FreqC/CHfx2; i++) {
//                if (i < bufferSize_Freq+bufferSize_FreqC/(2*CHfx2)) {
//                    SquareWave_freq_X[cfx][i+(bufferSize_FreqC/CHfx2)*m] = 3;
//                }
//                else{
//                    SquareWave_freq_X[cfx][i+(bufferSize_FreqC/CHfx2)*m] = 0;
//                }
//            }
//        }
//        std::cout << cfx << std::endl;
//        cfx++;
//        
//        
//        
//        CHf1--;
//        CHf2++;
//        if (cfx == LoopNum_FreqC) {
//            std::cout << "break FreqX" << std::endl;
//            break;
//        }
//    }

    

    
    
    
    std::cout << "データ作成終了" << std::endl;
    
    DAQmxBaseCreateTask("",&task.taskHandle);
    DAQmxBaseCreateAOVoltageChan(task.taskHandle,task.chan,"",task.min,task.max,DAQmx_Val_Volts,NULL);
    DAQmxBaseCfgSampClkTiming(task.taskHandle,"",task.sampleRate,DAQmx_Val_Rising,DAQmx_Val_ContSamps,task.samplesPerChan);
    DAQmxBaseSetWriteAttribute(task.taskHandle, DAQmx_Write_RegenMode, DAQmx_Val_DoNotAllowRegen);
    DAQmxBaseWriteAnalogF64(task.taskHandle,task.samplesPerChan,0,task.timeout,DAQmx_Val_GroupByChannel,ZeroVolData,&task.pointsWritten,NULL);
    DAQmxBaseStartTask(task.taskHandle);
    
    int pat = 0;
    int pattern = 0;
    int kbhit_tmp = 0;
    int t = 0;
    int disp = 0;
    n=0;
    ct = 0;
    cf = 0;
    int patcf = 0;
    x = 0;
    xt = 0;
    std::cout << "hajimari" << std::endl;

    while (1) {
        switch (pattern) {
            case 0:
                if (disp == pattern) {
                    std::cout << "case "<< pattern << std::endl;
                    disp++;
                }
                    for (int i = 0; i < 30; i++) {
                        DAQmxBaseWriteAnalogF64(task.taskHandle,task.test-(n*10),0,task.timeout,DAQmx_Val_GroupByChannel,SquareWave[n],&task.pointsWritten,NULL);
                    }
                    if (pat == 0) {
                        n++;
                        if (n == LoopNum) {
                            pat = 1;
                        }
                    }
                    if (pat == 1) {
                        n--;
                        if (n == 0) {
                            pat = 0;
                        }
                    }
                if (kbhit()) {
                    getchar();
                    pattern = pattern+1;
                }
                break;
                
            case 1:
                if (disp == pattern) {
                    std::cout << "case "<< pattern << std::endl;
                    disp++;
                    n = 0;
                }
                for (int i = 0; i < 30; i++) {
                    DAQmxBaseWriteAnalogF64(task.taskHandle,task.test-(n*10),0,task.timeout,DAQmx_Val_GroupByChannel,SquareWave[n],&task.pointsWritten,NULL);
                }
                n++;
                if (n == LoopNum) {
                    n = 0;
                }
                if (kbhit()) {
                    getchar();
                    pattern = pattern+1;
                }
                break;
                
            case 2:
                if (disp == pattern) {
                    std::cout << "case "<< pattern << std::endl;
                    disp++;
                    n = LoopNum-1;
                }
                for (int i = 0; i < 30; i++) {
                    DAQmxBaseWriteAnalogF64(task.taskHandle,task.test-(n*10),0,task.timeout,DAQmx_Val_GroupByChannel,SquareWave[n],&task.pointsWritten,NULL);
                }
                n--;
                if (n == 0) {
                    n = LoopNum-1;
                }
                if (kbhit()) {
                    getchar();
                    pattern = pattern+1;
                }
                break;
                
            case 3:
                if (disp == pattern) {
                    std::cout << "case "<< pattern << std::endl;
                    disp++;
                }
                DAQmxBaseWriteAnalogF64(task.taskHandle,bufferSize_Set,0,task.timeout,DAQmx_Val_GroupByChannel,Set_Square_wave[x],&task.pointsWritten,NULL);
                if (t == 0) {
                    std::cout << "番号[" << x << "]" << std::endl;
                    t++;
                }
                if (kbhit()) {
                    getchar();
                    x++;
                    t = 0;
                    if (x == 4) {
                        std::cout << "test" <<std::endl;
                        pattern = pattern+1;
                    }
                }
                break;
            
            case 4:
                if (disp == pattern) {
                    std::cout << "case "<< pattern << std::endl;
                    disp++;
                }

                for (int i = 0; i < 100; i++) {
                    DAQmxBaseWriteAnalogF64(task.taskHandle,bufferSize_Tim,0,task.timeout,DAQmx_Val_GroupByChannel,Tim_Square_wave[xt],&task.pointsWritten,NULL);
                }
                
                xt++;
                if (xt == LoopNum_Tim) {
                    
                    xt = 0;
                }
                if (kbhit()) {
                    getchar();
                    pattern = pattern+1;
                }
                break;
            
            case 5:
                if (disp == pattern) {
                    std::cout << "case "<< pattern << std::endl;
                    disp++;
                }
                DAQmxBaseWriteAnalogF64(task.taskHandle,bufferSize_FreqC,0,task.timeout,DAQmx_Val_GroupByChannel,SquareWave_freq_Change[cf],&task.pointsWritten,NULL);
                if (patcf == 0) {
                    cf++;
                    if (cf == LoopNum_FreqC) {
                        patcf = 1;
                    }
                }
                if (patcf == 1) {
                    cf--;
                    if (cf == 0) {
                        patcf = 0;
                    }
                }
                if (kbhit()) {
                    getchar();
                    pattern = pattern+1;
                }
                break;
                
            default:
                if (disp == pattern) {
                    std::cout << "case "<< pattern << std::endl;
                    disp++;
                    
                }
                kbhit_tmp = 1;
                DAQmxBaseWriteAnalogF64(task.taskHandle,bufferSize_Freq,0,task.timeout,DAQmx_Val_GroupByChannel,SquareWave_freq[0],&task.pointsWritten,NULL);
                break;
        }
        
        if (kbhit() && kbhit_tmp == 1) {
            ni_make(ZeroVolData);
            DAQmxBaseStopTask(task.taskHandle);
            DAQmxBaseClearTask(task.taskHandle);
            std::cout << "returnが入力されました．プロセスを終了します．" << getchar() << std::endl;
            break;
        }
    }
    for (int i = 0; i < LoopNum; i++) {
        delete[] SquareWave[i];
    }
    delete[] SquareWave;
    std::cout << "owari" << std::endl;
    return 0;
}